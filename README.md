# Resources
Resources for learning lang and framework ruby

### Pre
-  [Data Structure and Algorithm](https://www.geeksforgeeks.org/data-structures/)

### Software Design Principles
-   [S.O.L.I.D principles](https://robots.thoughtbot.com/back-to-basics-solid)
-   [Software Arcitecture](https://sourcemaking.com/)

### Text Editor & OS(Operating System)
-   [VIM Cheat Sheet](https://gist.github.com/ervinismu/dc438d3668dbacb04ab36c65c4fb5570)

### Git
-   [Book Git](https://books.goalkicker.com/GitBook/)
-   [Git Intro](https://git-scm.com/book/id/v1/Memulai-Git-Tentang-Version-Control)

### Ruby
-   [Book Ruby Basic](https://launchschool.com/books/ruby/read/introduction)
-   [Book Ruby OOP(Object Oriented Programming)](https://launchschool.com/books/oo_ruby/read/introduction)
-   [Ruby Code Convention](https://github.com/rubocop-hq/ruby-style-guide)
-   [Ruby Monk](https://rubymonk.com/)
-   [Hackerrank Ruby](https://www.hackerrank.com/domains/ruby)
-   [Codecademy Ruby](https://www.codecademy.com/learn/learn-ruby)
-   [Awesome Ruby Project](http://awesome-ruby.com/)
-   [IdRails](http://www.idrails.com/series)
-   [Railscasts](http://railscasts.com/)
-   [RubyHardWay](https://learnrubythehardway.org/book/)
-   [Ruby Guides](https://www.rubyguides.com/)

### Ruby on Rails
-   [Ruby on Rails Book](https://www.railstutorial.org/book)
-   [Ruby on rails api](https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one)

## Postgresql
-   [Book Postgresql](https://books.goalkicker.com/PostgreSQLBook/)
-   [Setup Postgresql](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04)

## CI/CD
- [.gitlab-ci.yml examples](https://docs.gitlab.com/ee/ci/examples/)
- [.gitlab-ci Variables](https://docs.gitlab.com/ee/ci/variables/)

## Rails Tutorials
- [Sending Email in Rails](https://launchschool.com/blog/handling-emails-in-rails)

## Server
- [Deploy Rails to Heroku](https://devcenter.heroku.com/articles/getting-started-with-rails5)
- [SSL Setup with Nginx + Certbot](https://www.digitalocean.com/community/tutorials/how-to-set-up-let-s-encrypt-with-nginx-server-blocks-on-ubuntu-16-04)
- [Install Vagrant Ubuntu](https://linuxize.com/post/how-to-install-vagrant-on-ubuntu-18-04/)
- [Vagrant vs Docker Concept](https://www.youtube.com/watch?v=9QGkJvbLpRA)
- [Setup nginx ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04)
- [Deploy Rails with Nginx and Puma](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-rails-app-with-puma-and-nginx-on-ubuntu-14-04)
- [Initial Server Setup Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04)